# vm-backup

% VM-BACKUP(1) Version 1.0 | Automated VM Backups

NAME
====

**vm-backup** — backs up all vm's on the local host

SYNOPSIS
========

| **vm-backup** \[**-v** _vm-name_]

DESCRIPTION
===========

Provides backups for VMs on the local host. If no _vm-name_ is provided,
it will attempt to backup all defined VMs on the local host. If a _vm-name_
is given with the **-v** option, only that VM will be backed up.

The following lists the steps vm-backup takes:
 1. If the VM is running, suspend it.
 2. Get a list of disks the VM is using and rsync them to the **ARCHIVE_HOST**
 3. Resume the VM if it was previously running.

Options
-------

-h, --help

:   Prints brief usage information.

-v

:   The name of the VM to be backed up, or restored,  as show by virsh list.
    If the **-v** option is not given during backup, all VMs will be backed up.

ENVIRONMENT
===========

**ARCHIVE_HOST**

:   The host where backups will be stored. If $ARCHIVE_HOST is not set,
    a default host (gordolix-10ge-direct) will be used.

BUGS
====

See GitLab Issues: <https://gitlab.com/jgtaylor/vm-backup/issues>

AUTHOR
======

Joshua Taylor <taylor@ifi.csg.uzh.ch>

SEE ALSO
========

**virsh(1)**, **rsync(1)**

