# vm-restore

% VM-RESTORE(1) Version 1.0 | Automated VM Backups

NAME
====

**vm-restore** — restores vm's on the local host

SYNOPSIS
========

| **vm-restore** **-v** _vm-name_

DESCRIPTION
===========

**vm-restore** will restore a Virtual Machine's disk image to its
original loction.

In the event that the Virtual Machine has been deleted, and a backup
exists, the VM will be recreated as it had been defined (via the
output of **virsh dumpxml vm_name**).

Options
-------

-h, --help

:   Prints brief usage information.

-v

:   The name of the VM to be restored,  as show by virsh list. If the
    **-v** option is not given during backup, all VMs will be backed up.

ENVIRONMENT
===========

**ARCHIVE_HOST**

:   The host where backups are stored. If $ARCHIVE_HOST is not set,
    a default host (gordolix-10ge-direct) will be used.

BUGS
====

See GitLab Issues: <https://gitlab.com/jgtaylor/vm-backup/issues>

AUTHOR
======

Joshua Taylor <taylor@ifi.csg.uzh.ch>

SEE ALSO
========

**virsh(1)**, **rsync(1)**, **vm-backup(1)**

