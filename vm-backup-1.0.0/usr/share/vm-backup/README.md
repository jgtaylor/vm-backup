# VM Backup & Restore

This is a pair of bash scripts to backup and restore VM disk images and VM XML configurations.

## VM Backup

The script called with no arguments attempts to backup all known VMs avaiable via `virsh list`.

The only valid arguments are `-v vm-name` and `-a`, where `-a` must also have `-v vm-name`.

Without the `-a` argument, it is a normal backup. However, with the `-a` argument, it will create an untracked archive image in a subdirectory named `archive`.

During the backup, a running VM will be suspended, and resumed upon the backup's completion.

In the case of all VMs being backed up, it will happen serially. Should parallelism be desired, that can be achived via:
```shell
for VM in $(virsh list --name); do
    vm-backup -v ${VM}
done
```
Note that the calling user must be root or otherwise able to read & write the VM Disk images both on the Hypervisor and on the storage server.

Note that this has never been tested in an SELinux environment.

### Backup Retention

As of this writing, the retention is set at 4 copies. The duration of the retetion is dependent upon how often backups are made. For example, if backups are made once a week, there should be 4 versions, spanning roughly one month.

The retention can be changed by modifying the script variable `RETENTION=4`.

### Backup Directory Structure

As of this writing, all backups are stored on `gordolix-10ge-direct` (or, just gordolix). The structure should look like the following:
```
/mnt/md1/backup/
            |
            +-vm-name/
                |
                +-archive/
                |   |-disk.img.unix_timestamp
                |   |-disk.qcow2.unix_timestamp
                |   |-vm-name.xml.unix_timestamp
                |
                |-disk.img.unix_timestamp
                |-disk.qcow2.unix_timestamp
                |-vm-name.xml.unix_timestamp
```

## VM Restore

The script must be called with the `-v vm-name` argument. If no name is given, the script will inform you of that and exit.

If the VM to restore is currently running, it will be shutdown and then restored.

In the case that a VM has been deleted from the Hypervisor, but it has previously been backed up, a full restore will occur along with the creation of the VM (persistent) as it was defined at the time of the backup. After restore, the newly re-created VM will be automatically booted.

The user will be given a choice, by date, of the backup they would like restored.

Given a uniform environment - that is, common network infrastructure & disk locations - `vm-restore` can be used to move a VM from one Hypervisor to another, in a kind of poor-man's 'migration'.